from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from .internal.price_update import generate_price_obj

from .database import db

from .models import Stock as ModelStock, create_price_stock_view
from .models import Price as ModelPrice


app = FastAPI(title="Generated Stocks API")


@app.on_event("startup")
async def startup():
    await db.connect()
    await create_price_stock_view()

@app.on_event("startup")
@repeat_every(seconds=1, raise_exceptions=True)
async def update_stock_prices() -> None:
    stocks = await ModelStock.get_stocks_and_prices()
    records = stocks if len(stocks) > 0 else await ModelStock.get_stocks()
    for record in records:
        stock = dict(record)
        price = await generate_price_obj(stock)
        if price.get("current", 0) >= 0:
            await ModelPrice.create_price(**price)
    

@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()