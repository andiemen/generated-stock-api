"""
Randomly returns an incremental or
decremental price"""

from random import choice, randint

async def generate_movement() -> int:
    movement = -1 if randint(0,1) < 0.5 else 1
    return movement

async def generate_price_obj(stock: dict) -> dict:
    if not stock: return
    stock_id = stock.get("stock_id") or stock.get("id")
    generated_price = await generate_movement()
    new_price = generated_price + stock.get("price", 0)
    new_price = new_price if new_price > 0 else 0
    price = {
        "current": new_price,
        "stock_id": stock_id
    }
    return price
