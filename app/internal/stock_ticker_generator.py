"""
Generates random up to four (4) characters
(unless specified in source) capitalized
achronyms similar to stock tickers.
e.g. A
MSX
"""

import string
from random import choice, randint
from typing import Generator



def generate_ticker(source: str, limit: int) -> Generator:
    """Generates a random string of characters
    from source string till limit.
    
    source - string of characters to generate from
    limit - number of tickers to generate
    """
    source = string.ascii_uppercase if not source or not source.strip() else source
    
    while limit > 0:
        yield ''.join(choice(source) for _ in range(randint(1,4)))
        limit -= 1

def generate_tickers_unique(source: str, count: int) -> list[str]:
    """Returns a list of unique ticker(s) generated from
    generate_ticker() function"""

    tickers = []
    while len(tickers) < count:
        ticker = next(generate_ticker(source, 1))
        if ticker not in tickers:
            tickers.append(ticker)
    
    return tickers
