from datetime import datetime
from pydantic import BaseModel

class Stock(BaseModel):
    stock_id: int
    ticker: str
    price: int
    description: str = None
    last_update: datetime

    class Config:
        orm_mode = True

class Price(BaseModel):
    current: int
    stock_id: int
    created_at: datetime

    class Config:
        orm_mode = True
