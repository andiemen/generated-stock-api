import uvicorn
from .models import Stock as ModelStock
from .schema import Stock as SchemaStock
from .app import app


@app.get("/api/stocks/", response_model=list[SchemaStock])
async def get_stocks():
    stocks = await ModelStock.get_stocks_and_prices()
    return stocks # [SchemaStock(**stock_record).dict() for stock_record in stocks]

@app.get("/api/stock/{ticker}", response_model=SchemaStock)
async def get_stock_by_ticker(ticker: str):
    stock = await ModelStock.get_stock_and_price_by_ticker(ticker)
    return stock

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
