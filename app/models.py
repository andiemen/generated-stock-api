from sqlalchemy import ForeignKey, Table, Column, String, Integer, DateTime, text
from sqlalchemy_views import CreateView

from .database import db, metadata

Stock_id = int

stocks = Table(
    "stocks",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("ticker", String(length=10), unique=True, nullable=False),
    Column("description", String),
    Column("created_at", DateTime, server_default=text("NOW()")),
    Column("updated_at", DateTime),
    Column("deleted_at", DateTime),
)

prices = Table(
    "prices",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("current", Integer, nullable=False),
    Column("stock_id", Integer, ForeignKey("stocks.id"), nullable=False),
    Column("created_at", DateTime, server_default=text("NOW()")),
    Column("deleted_at", DateTime),
)

stock_price_view = Table(
    'stock_price_view',
    metadata,
    Column("stock_id", Integer),
    Column("ticker", String(length=10)),
    Column("description", String),
    Column("price", Integer),
    Column("last_update", DateTime),
)

async def create_price_stock_view():
    """Creates a view from stock and price tables"""
    definition = text(
        """
        SELECT stocks.id as stock_id, stocks.ticker, stocks.description, 
        COALESCE(prices.current, 0) as price, 
        prices.created_at as last_update
        FROM stocks
        JOIN (SELECT DISTINCT ON (stock_id) *
            FROM prices
            ORDER BY stock_id, created_at DESC
        ) prices
        ON prices.stock_id = stocks.id
        """
    )

    create_view = CreateView(stock_price_view, definition, or_replace=True)
    await db.execute(create_view)
class Stock:
    @classmethod
    async def get_stocks(cls):
        query = stocks.select().where(stocks.c.deleted_at == None)
        stock = await db.fetch_all(query)
        return stock

    @classmethod
    async def get_stocks_and_prices(cls):
        query = stock_price_view.select()
        stock = await db.fetch_all(query)
        return stock

    @classmethod
    async def get_stock_by_id(cls, id: Stock_id):
        query = stock_price_view.select().where(stock_price_view.c.stock_id == id)
        stock = await db.fetch_one(query)
        return stock
    
    @classmethod
    async def get_stock_and_price_by_ticker(cls, ticker: str):
        query = stock_price_view.select().where(stock_price_view.c.ticker == ticker.upper())
        stock = await db.fetch_one(query)
        return stock

class Price:
    @classmethod
    async def get_current_price(stock_id: Stock_id):
        query = f"""
                SELECT DISTINCT ON (stock_id) *
                FROM prices
                WHERE stock_id = {stock_id}
                ORDER BY stock_id, created_at DESC
                FETCH FIRST 1 ROWS ONLY
                """
        price = await db.execute(query)
        return price
    
    @classmethod
    async def create_price(cls, **price):
        query = prices.insert().values(**price)
        price_id = await db.execute(query)
        return price_id
